// Marco Antonio Fidencio Chávez Fuentes 201020831
function fibonacci(limit) {
    if (limit <= 0) return 'Error: Número positivo es requerido'
    if (limit < 2) return limit
    else return fibonacci(limit - 1) + fibonacci(limit - 2)
}

// Francisco Javier Bran Fuentes - 201730555
function primo(n) {
    if(n < 0) return "Error: Numero natural requerido."
    if(n == 0 || n == 1) return false;
    for(var aux = 2; aux <= (n / 2); aux++) {
      if(n % aux == 0) return false;
    } 
    return true;
}