//José Carlos I Alonzo Colocho 201700965
function palindromo(palabra){
    palabra = palabra.replace(/\s/g, "").toLowerCase()
    let invertida = palabra.split("").reverse().join("")
    return palabra == invertida
}