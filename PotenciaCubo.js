//José Orlando Wannan Escobar - 201612331
function Potencia_Cubo(valor)
{
	if(Number.isInteger(valor)) return Math.pow(valor,3);
	else return 0;
}