function Multiplicacion(valor1, valor2) {
    if ((typeof valor1 == 'number') && (typeof valor2 == 'number')) {
        var resultado = valor1 * valor2
        console.log("El resultado de la multiplicacion es: " + valor1 * valor2)
        if (Number.isInteger(resultado)) {
            console.log("Tipo: Entero")
        } else {
            console.log("Tipo: Decimal")
        }
    } else {
        console.log("los datos ingresados deben ser números")
    }
}
Multiplicacion(5, 59.10)
function Division(valor1, valor2) {
    if ((typeof valor1 == 'number') && (typeof valor2 == 'number')) {
        if (valor2 != 0) {
            var resultado = valor1 / valor2
            console.log("El resultado de la division es: " + valor1 / valor2)
            if (Number.isInteger(resultado)) {
                console.log("Tipo: Entero")
            } else {
                console.log("Tipo: Decimal")
            }
        }else{
            console.log("No se puede dividir entre 0")
        }
    } else {
        console.log("los datos ingresados deben ser números")
    }

}
Division(100, 0)